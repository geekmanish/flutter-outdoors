import 'package:flutter/material.dart';
import 'package:punehikers/constants/var.dart';
import 'package:punehikers/models/place.dart';

class PlaceRow extends StatelessWidget {
  final Place _place;
  final Function _tapHandler;
  final Widget _action;
  final Function _actionHandler;
  PlaceRow(this._place, [this._tapHandler, this._action, this._actionHandler]);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: InkWell(
        child: Row(children: [
          Padding(
            child: FadeInImage(
                width: listIconSize,
                height: listIconSize,
                fit: BoxFit.cover,
                placeholder: AssetImage(splashImage),
                image: NetworkImage(_place.image)),
            padding: EdgeInsets.only(
              right: 8.0,
            ),
          ),
          Expanded(
              child: Text(
            _place.name,
            style: TextStyle(
              fontSize: biggerFontSize,
            ),
          ))
        ]),
        onTap: () {
          if (_tapHandler != null) {
            _tapHandler(_place);
          }
        },
      ),
      trailing: this._action,
      onTap: () {
        if (_actionHandler != null) {
          _actionHandler(_place);
        }
      },
    );
  }
}
