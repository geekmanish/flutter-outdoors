import 'package:flutter/material.dart';
import 'package:punehikers/constants/var.dart';
import 'package:punehikers/models/place.dart';
import 'package:url_launcher/url_launcher.dart';

class PlaceDetails extends StatelessWidget {
  final Place _place;
  final bool showName;
  final bool showDetails;
  final bool showLink;
  final bool showImage;
  final double imageHeight;
  PlaceDetails(
    this._place, {
    this.showImage = true,
    this.showName = false,
    this.showDetails = true,
    this.showLink = true,
    this.imageHeight,
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        showImage ? Row(
          children: <Widget>[
            Expanded(
              child: Image.network(_place.image,
                  height: imageHeight ?? detailsImageHeight, fit: BoxFit.cover),
            ),
          ],
        ) : null,
        showName
            ? Padding(
                child: Text(
                  _place.name,
                  style: TextStyle(
                    fontSize: biggerFontSize,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                padding: EdgeInsets.only(
                    left: 8.0, right: 8.0, top: 4.0, bottom: 4.0),
              )
            : null,
        showDetails
            ? Padding(
                child: Text(
                  _place.shortDescription,
                ),
                padding: EdgeInsets.only(
                    left: 8.0, right: 8.0, top: 4.0, bottom: 4.0),
              )
            : null,
        showDetails
            ? Padding(
                child: Text(
                  'Details:',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                padding: EdgeInsets.only(
                    left: 8.0, right: 8.0, top: 4.0, bottom: 4.0),
              )
            : null,
        showDetails
            ? Padding(
                child: Text(
                  _place.description,
                ),
                padding: EdgeInsets.only(
                    left: 8.0, right: 8.0, top: 4.0, bottom: 4.0),
              )
            : null,
        showLink
            ? Padding(
                child: Row(
                  children: <Widget>[
                    Text('Link: '),
                    InkWell(
                      child: Text(_place.wikiLink,
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                          )),
                      onTap: () async {
                        if (await canLaunch(_place.wikiLink)) {
                          await launch(_place.wikiLink);
                        } else {
                          throw 'Could not launch ${_place.wikiLink}';
                        }
                      },
                    )
                  ],
                ),
                padding: EdgeInsets.only(
                    left: 8.0, right: 8.0, top: 4.0, bottom: 4.0),
              )
            : null
      ].where((widget) => widget != null).toList(),
    );
  }
}
