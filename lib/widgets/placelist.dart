import 'package:flutter/material.dart';
import 'package:punehikers/constants/var.dart';
import 'package:punehikers/models/bottomnavtab.dart';
import 'package:punehikers/models/place.dart';
import 'package:punehikers/screens/addplace.dart';
import 'package:punehikers/screens/mapscreen.dart';
import 'package:punehikers/screens/placedetailsscreen.dart';
import 'package:punehikers/screens/savedscreen.dart';
import 'package:punehikers/utils/api.dart';
import 'package:punehikers/widgets/Loader.dart';
import 'package:punehikers/widgets/placerow.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PlaceListState extends State<PlaceList> {
  final _places = <Place>[];
  final Set<Place> _savedPlaces = Set<Place>();
  bool loading = false;
  final String _apiUrl = 'https://api.jsonbin.io/b/5e2812265df640720839eb38/2';
  final String _savedPlacesPrefKey = 'savedPlaces';
  SharedPreferences _prefs;
  List<BottomNavTab> tabs = [
    BottomNavTab(Icons.list, LANG['PLACES']),
    BottomNavTab(Icons.bookmark_border, LANG['SAVED']),
    BottomNavTab(Icons.map, LANG['MAP']),
  ];

  @override
  void initState() {
    super.initState();
    _getPlaces();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(LANG['PLACES']),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add_circle_outline),
            onPressed: () {
              _pushAddPlaceScreen();
            },
          )
        ],
      ),
      body: loading ? Loader('Loading places') : _buildSuggestions(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 0,
        items: tabs.map((tab) => BottomNavigationBarItem(
            icon: Icon(tab.icon), title: Text(tab.title))).toList(),
        onTap: (int index) {
          if (index == 1) {
            _pushSavedScreen();
          } else if (index == 2) {
            _pushMapScreen();
          }
        },
      ),
    );
  }

  void _initSavedPlaces() async {
    _prefs = await SharedPreferences.getInstance();
    _prefs.getStringList(_savedPlacesPrefKey)?.forEach((placeId) {
      _places.forEach((place) {
        if (place.id.toString() == placeId) {
          _savedPlaces.add(place);
        }
      });
    });
  }

  void _getPlaces() async {
    setState(() => loading = true);
    await Api.call(_apiUrl, (final status, final body) {
      setState(() => loading = false);
      if (status == 200) {
        var places = body['places'];
        for (final place in places) {
          setState(() {
            _places.add(Place.fromObj(place));
          });
        }
        _initSavedPlaces();
      }
    }, parseJson: true);
  }

  void _pushAddPlaceScreen() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (BuildContext context) {
        return AddPlaceScreen();
      }),
    );
  }

  void _pushMapScreen() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
          builder: (BuildContext context) => MapScreen(_places, _pushDetails)),
    );
  }

  void _pushSavedScreen() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
          builder: (BuildContext context) =>
              SavedScreen(_savedPlaces, _pushDetails)),
    );
  }

  void _pushDetails(place) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
          builder: (BuildContext context) => PlaceDetailsScreen(place)),
    );
  }

  Widget _buildSuggestions() {
    return ListView.builder(
      key: Key('itemList'),
      padding: const EdgeInsets.all(16.0),
      itemBuilder: (context, i) {
        if (i.isOdd) return Divider();
        final index = i ~/ 2;
        if (index >= _places.length) {
          return null;
        }
        final bool alreadySaved = _savedPlaces.contains(_places[index]);
        return PlaceRow(
          _places[index],
          _pushDetails,
          Icon(
            alreadySaved ? Icons.favorite : Icons.favorite_border,
            color: alreadySaved ? primarySwatch.shade500 : null,
          ),
          _toggleSaved,
        );
      },
    );
  }

  _toggleSaved(place) {
    final bool alreadySaved = _savedPlaces.contains(place);
    setState(() {
      alreadySaved ? _savedPlaces.remove(place) : _savedPlaces.add(place);
      _updateSavedItemsPrefs();
    });
  }

  void _updateSavedItemsPrefs() async {
    final placeIds = <String>[];
    _savedPlaces.forEach((place) {
      placeIds.add(place.id.toString());
    });
    await _prefs?.setStringList(_savedPlacesPrefKey, placeIds);
  }
}

class PlaceList extends StatefulWidget {
  @override
  PlaceListState createState() => PlaceListState();
}