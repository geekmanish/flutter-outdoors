import 'package:flutter/material.dart';

class Loader extends StatelessWidget {
  final String _loadingText;
  Loader(this._loadingText);
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(
                  child: CircularProgressIndicator(),
                  padding: EdgeInsets.only(bottom: 16.0),
                ),
                Text((_loadingText ?? 'Loading') + '...')
              ],
            )
          ],
        )
      ],
    );
  }
}
