import 'package:flutter/material.dart';

final primarySwatch = Colors.green;
final double detailsImageHeight = 320;
final double listIconSize = 48;
final String splashImage = 'lib/assets/outline.png';
final biggerFontSize = 18.0;
final String mapIconImage = 'mountain-15';

const LANG = {
  'WELCOME_TO_APP': 'Welcome to PuneHikers',
  'ENTER_YOUR_EMAIL': 'Enter your email',
  'ENTER_YOUR_PASSWORD': 'Enter your password',
  'PLACES': 'PLACES',
  'SAVED': 'SAVED',
  'MAP': 'MAP',
  'SAVED_PLACES': 'SAVED PLACES',
  'NOTHING_HERE_YET': 'There\'s nothing here yet.',
  'START_BY_SAVING_PLACES': 'Start by saving some places.',
  'MAP_VIEW': 'Map View',
  'SIGN_IN': 'Sign In',
  'ADD_A_PLACE': 'Suggest A Place',
};
