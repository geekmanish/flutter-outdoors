import 'dart:convert';
import 'package:http/http.dart' as http;

class Api {
  static call(
    url,
    cb, {
    requestMethod = 'get',
    parseJson = false,
    callback = null,
  }) async {
    switch (requestMethod) {
      case 'get':
        var res = await http.get(url);
        cb(res.statusCode, parseJson ? json.decode(res.body) : res.body);
    }
  }
}
