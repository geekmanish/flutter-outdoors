import 'package:flutter/material.dart';
import 'package:punehikers/screens/splashscreen.dart';
import 'constants/var.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Places',
      theme: ThemeData(
        primarySwatch: primarySwatch,
      ),
      home: SplashScreen(),
    );
  }
}