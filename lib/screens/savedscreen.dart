import 'package:flutter/material.dart';
import 'package:punehikers/constants/var.dart';
import 'package:punehikers/models/place.dart';
import 'package:punehikers/widgets/placerow.dart';

class SavedScreen extends StatelessWidget {
  final Set<Place> _savedPlaces;
  final Function _tapHandler;
  SavedScreen(this._savedPlaces, this._tapHandler);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(LANG['SAVED_PLACES']),
      ),
      body: _savedPlaces.length > 0
          ? ListView(
              children: ListTile.divideTiles(
              context: context,
              tiles: _savedPlaces.map((Place place) {
                return PlaceRow(place, _tapHandler);
              }),
            ).toList())
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                    child: Column(
                  children: <Widget>[
                      Image.asset(splashImage,
                          height: 128, width: 128, fit: BoxFit.contain),
                    Text(
                      LANG['NOTHING_HERE_YET'] +
                          ' ' +
                          LANG['START_BY_SAVING_PLACES'],
                      style: TextStyle(fontSize: 18.0),
                    ),
                  ],
                ))
              ],
            ),
    );
  }
}
