import 'package:flutter/material.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:punehikers/constants/var.dart';
import 'package:punehikers/models/place.dart';
import 'package:punehikers/widgets/placedetails.dart';

class MapScreen extends StatelessWidget {
  final List<Place> _places;
  final Map<String, int> _symbols = Map<String, int>();
  final Function _onPlaceTap;
  MapScreen(this._places, this._onPlaceTap);

  @override
  Widget build(BuildContext context) {
    MapboxMapController _map;
    return Scaffold(
        appBar: AppBar(title: Text(LANG['MAP_VIEW'])),
        body: Container(
            child: Column(
          children: <Widget>[
            Expanded(
              child: MapboxMap(
                  onStyleLoadedCallback: () {
                    _places.forEach((Place place) async {
                      Symbol symbol = await _map?.addSymbol(SymbolOptions(
                          geometry: LatLng(place.lat, place.lng),
                          iconSize: 1.5,
                          iconImage: mapIconImage));
                      _symbols[symbol.id] = place.id;
                    });
                    _map?.onSymbolTapped?.add((Symbol symbol) {
                      if (_symbols.containsKey(symbol.id)) {
                        Place place = _places.firstWhere(
                            (place) => place.id == _symbols[symbol.id]);
                        print('Place: $place');
                        if (place != null) {
                          showModalBottomSheet(
                              context: context,
                              builder: (BuildContext bc) {
                                return Column(
                                  children: <Widget>[
                                    PlaceDetails(place, showName: true),
                                    OutlineButton(
                                      child: Text('View More'),
                                      onPressed: () => _onPlaceTap(place),
                                    )
                                  ],
                                );
                              });
                        }
                      }
                    });
                  },
                  onMapCreated: (map) {
                    _map ??= map;
                  },
                  initialCameraPosition: CameraPosition(
                    target: LatLng(18.52, 73.85),
                    zoom: 8,
                  )),
            )
          ],
        )));
  }
}
