import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:punehikers/constants/var.dart';
import 'package:punehikers/screens/homescreen.dart';
import 'package:splashscreen/splashscreen.dart' as SS;

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SS.SplashScreen(
        seconds: 2,
        navigateAfterSeconds: new HomeScreen(),
        title: new Text(
          LANG['WELCOME_TO_APP'],
          style: TextStyle(fontSize: biggerFontSize),
        ),
        image: Image.asset(splashImage,
            height: 128, width: 128, fit: BoxFit.contain),
        backgroundColor: primarySwatch.shade50,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        loaderColor: primarySwatch.shade900);
  }
}
