import 'package:flutter/material.dart';
import 'package:punehikers/constants/var.dart';
import 'package:punehikers/models/place.dart';
import 'package:punehikers/widgets/placedetails.dart';

class ReportPlaceScreen extends StatefulWidget {
  final Place _place;
  ReportPlaceScreen(this._place);
  @override
  State<StatefulWidget> createState() => ReportPlaceScreenState(this._place);
}

class ReportPlaceScreenState extends State<ReportPlaceScreen> {
  final Place _place;
  final _key = GlobalKey<FormState>();
  bool _isValid = false;
  ReportPlaceScreenState(this._place);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Report :: ' + _place.name)),
      resizeToAvoidBottomPadding: true,
      body: Container(
        child: Column(
            children: <Widget>[
              PlaceDetails(_place, imageHeight: detailsImageHeight * .75),
              Padding(
                padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 8.0),
                child: Form(
                  onChanged: () {
                    setState(()  => _isValid = _key.currentState.validate());
                  },
                  key: _key,
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        maxLines: 4,
                        decoration: InputDecoration(labelText: 'Why should this be removed?'),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Report must not be empty';
                          } else if (value.length < 16) {
                            return 'Report too short';
                          }
                          return null;
                        },
                      ),
                      OutlineButton(
                        child: Text('Submit Report'),
                        onPressed: _isValid ? () {

                        } : null,
                      )
                    ],
                  ),
                ),
              )
            ]
        ),
      ),
    );
  }
}
