import 'package:flutter/material.dart';
import 'package:punehikers/models/place.dart';
import 'package:punehikers/screens/reportplacescreen.dart';
import 'package:punehikers/widgets/placedetails.dart';

class PlaceDetailsScreen extends StatelessWidget {
  final Place _place;
  final List<String> _menuItems = ['Report'];
  PlaceDetailsScreen(this._place);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_place.name),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected:(String menuItem) {
              _menuClicked(context, menuItem);
            },
            itemBuilder: (BuildContext bc) {
              return _menuItems
                  .map((String menuItem) => PopupMenuItem(
                        value: menuItem,
                        child: Text(menuItem),
                      ))
                  .toList();
            },
          )
        ],
      ),
      body: PlaceDetails(_place),
    );
  }

  _menuClicked(context, String menuItem) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (BuildContext context) {
        return ReportPlaceScreen(_place);
      }),
    );
  }
}
