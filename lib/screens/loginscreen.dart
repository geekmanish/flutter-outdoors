import 'package:flutter/material.dart';
import 'package:punehikers/constants/var.dart';
import 'package:punehikers/screens/homescreen.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(LANG['SIGN_IN'])),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.chevron_right),
        onPressed: () {
          Navigator.pushReplacement(context,
              MaterialPageRoute<void>(builder: (BuildContext context) {
            return HomeScreen();
          }));
        },
      ),
      body: Padding(
          child: Column(
            children: <Widget>[
              Center(
                  child: Image.asset(splashImage,
                      height: 128, width: 128, fit: BoxFit.contain)),
              Row(
                children: <Widget>[
                  Expanded(
                      child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration:
                        InputDecoration(labelText: LANG['ENTER_YOUR_EMAIL']),
                  ))
                ],
              ),
              Row(
                children: <Widget>[
                  Expanded(
                      child: TextFormField(
                    decoration:
                        InputDecoration(labelText: LANG['ENTER_YOUR_PASSWORD']),
                  ))
                ],
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          padding: EdgeInsets.all(32.0)),
    );
  }
}
