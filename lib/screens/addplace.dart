import 'package:flutter/material.dart';
import 'package:punehikers/constants/var.dart';
import 'package:validators/validators.dart' as validator;

class AddPlaceScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AddPlaceScreenState();
}

class AddPlaceScreenState extends State<AddPlaceScreen> {
  final _key = GlobalKey<FormState>();
  bool _isValid = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(LANG['ADD_A_PLACE'])),
        body: Padding(
          child: Form(
              key: _key,
              onChanged: () =>
                  setState(() => _isValid = _key.currentState.validate()),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Enter place name'),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Name is required';
                      } else if (value.length > 32) {
                        return 'Name is too long';
                      } else if (value.length < 3) {
                        return 'Name is too short';
                      } else if (validator.matches(value, "[^a-zA-Z ]")) {
                        return 'Invalid name';
                      }
                      return null;
                    },
                    maxLength: 64,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.url,
                    decoration:
                        InputDecoration(labelText: 'Enter wikipedia link'),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Link is required';
                      } else if (value.length < 3) {
                        return 'Link too short';
                      } else if (!validator.isURL(value)) {
                        return 'Link is invalid';
                      }
                      return null;
                    },
                    maxLength: 512,
                  ),
                  TextFormField(
                    decoration: InputDecoration(labelText: 'Enter your name'),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Name is required';
                      } else if (value.length > 32) {
                        return 'Name is too long';
                      } else if (value.length < 3) {
                        return 'Name is too short';
                      } else if (validator.matches(value, "[^a-zA-Z ]")) {
                        return 'Invalid name';
                      }
                      return null;
                    },
                    maxLength: 32,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    decoration:
                        InputDecoration(labelText: 'Enter your contact number'),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Number is required';
                      } else if (value.length > 32) {
                        return 'Number too long';
                      } else if (value.length < 3) {
                        return 'Number too short';
                      } else if (validator.matches(value, "[^0-9 \+]")) {
                        return 'Invalid name';
                      }
                      return null;
                    },
                    maxLength: 32,
                  ),
                  Padding(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Note',
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        Expanded(
                          child: Text(
                              ': This information will be verified by a moderator.'),
                        )
                      ],
                    ),
                    padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Builder(
                          builder: (ctx) {
                            return RaisedButton(
                              child: Text('Submit'),
                              color: primarySwatch.shade500,
                              disabledColor: primarySwatch.shade50,
                              onPressed: _isValid
                                  ? () {
                                      Scaffold.of(ctx).showSnackBar(
                                          SnackBar(content: Text('Snackbar')));
                                    }
                                  : null,
                            );
                          },
                        ),
                      )
                    ],
                  )
                ],
              )),
          padding: EdgeInsets.all(16.0),
        ));
  }
}
