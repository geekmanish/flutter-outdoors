import 'dart:convert';

class Place {
  int _id;
  String _name;
  String _shortDescription;
  String _description;
  String _image;
  String _wikiLink;
  double _lat;
  double _lng;

  /// named constructor
  Place.fromJSON(jsonStr) {
    final placeObj = json.decode(jsonStr);
    _fromObj(placeObj);
  }

  /// named constructor
  Place.fromObj(placeObj) {
    _fromObj(placeObj);
  }

  _fromObj(placeObj) {
    this._id = placeObj['id'];
    this._name = placeObj['name'];
    this._shortDescription = placeObj['shortDescription'];
    this._description = placeObj['description'];
    this._image = placeObj['image'];
    this._wikiLink = placeObj['wikiLink'];
    this._lat = placeObj['lat'];
    this._lng = placeObj['lng'];
  }

  /// getters
  get id => _id;
  get description => _description;
  get name => _name;
  get image => _image;
  get shortDescription => _shortDescription;
  get lat => _lat;
  get lng => _lng;
  get wikiLink => _wikiLink;
}
