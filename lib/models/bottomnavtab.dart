
import 'package:flutter/material.dart';

class BottomNavTab {
  IconData icon;
  String title;
  BottomNavTab(this.icon, this.title);
}
