import 'package:flutter_driver/driver_extension.dart';
import 'package:punehikers/main.dart' as app;

void main() {
  enableFlutterDriverExtension();
  app.main();
}
